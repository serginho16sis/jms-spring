package br.com.pocs.service;

import javax.jms.Destination;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import br.com.pocs.model.Produto;

@Service
public class EstoqueService {
	
	private JmsTemplate jmsTemplate;
	private Destination estoqueDestination;

	public EstoqueService(JmsTemplate jmsTemplate, Destination estoqueDestination) {
		this.jmsTemplate = jmsTemplate;
		this.estoqueDestination = estoqueDestination;
	}

	public void registrarSaida(Produto produto) {
		jmsTemplate.convertAndSend(estoqueDestination, produto);
	}
	
	
	public void registrarEntrada(Produto produto) {
		/**
		 * Código de mentira que registra uma entrada
		 * Adiciona mais codigo, que dessa resolve
		 */
	}
	
}
