package br.com.pocs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.JmsAutoConfiguration;

@SpringBootApplication
@EnableAutoConfiguration(exclude=JmsAutoConfiguration.class)
public class JmsSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(JmsSpringApplication.class, args);
	}
}
