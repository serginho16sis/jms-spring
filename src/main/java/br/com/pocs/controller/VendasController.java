package br.com.pocs.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.pocs.model.Produto;
import br.com.pocs.service.EstoqueService;

@RestController
@RequestMapping("/vendas")
public class VendasController {
	
	private EstoqueService estoqueService;
	
	public VendasController(EstoqueService estoqueService) {
		this.estoqueService = estoqueService;
	}

	@PostMapping
	public ResponseEntity vender(@RequestBody Produto produto) {
		estoqueService.registrarSaida(produto);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

}
