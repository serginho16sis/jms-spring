package br.com.pocs.config;

import java.util.Properties;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.naming.Context;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.jndi.JndiTemplate;

@Configuration
public class WebLogicJmsConfiguration {
	
	
	@Bean
    public JndiTemplate jndiTemplate() {
		
		Properties env = new Properties();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
        env.put(Context.PROVIDER_URL, "t3://meuservidor:porta");
        env.put(Context.SECURITY_PRINCIPAL, "usuario");
        env.put(Context.SECURITY_CREDENTIALS, "senha");
        
		JndiTemplate jndiTemplate = new JndiTemplate();
		jndiTemplate.setEnvironment(env);
        return jndiTemplate;
    }

	@Bean
	public MessageConverter jacksonJmsMessageConverter() {
		MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
		converter.setTargetType(MessageType.TEXT);
		converter.setTypeIdPropertyName("_type");
		return converter;
	}

	@Bean
	public JndiObjectFactoryBean queueConnectionFactory() {
		JndiObjectFactoryBean queueConnectionFactory = new JndiObjectFactoryBean();
		queueConnectionFactory.setJndiTemplate(jndiTemplate());
		queueConnectionFactory.setJndiName("jms/bureauBatchConnectionFactory");
		return queueConnectionFactory;
	}

	@Bean
	public JndiObjectFactoryBean jmsQueue() {
		JndiObjectFactoryBean jmsQueue = new JndiObjectFactoryBean();
		jmsQueue.setJndiTemplate(jndiTemplate());
		jmsQueue.setJndiName("jms/receitaFederalQueue");

		return jmsQueue;
	}

	@Bean
	@Qualifier("estoqueDestination")
	public Destination estoqueDestination() {
		Destination destination = (Destination) jmsQueue().getObject();
		return destination;
	}

	@Bean
	public JmsTemplate queueSenderTemplate() {
		JmsTemplate jmsTemplate = new JmsTemplate();
		jmsTemplate.setMessageConverter(jacksonJmsMessageConverter());
		jmsTemplate.setConnectionFactory((ConnectionFactory) queueConnectionFactory().getObject());
		jmsTemplate.setReceiveTimeout(5000);
		return jmsTemplate;
	}

}
